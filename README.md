# poc-scheduler

proof of concept dari scheduler dengan menerima parameter cron expression dari database

## database

buat database pada postgresql dengan nama 'config-schedule'

## API

```
{{url}}/update
```
untuk update value cron pada database dengan body:

```javascript
{
	"name": "job1/job2",
	"value": "cron expression"
}
```

atau bisa langsung update melalui database nya

```
{{url}}/restart
```
untuk restart job setelah proses update database