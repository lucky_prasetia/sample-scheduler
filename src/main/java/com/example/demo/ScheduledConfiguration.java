package com.example.demo;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.concurrent.ScheduledFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronTrigger;

@Configuration
@EnableScheduling
public class ScheduledConfiguration implements SchedulingConfigurer {
	
	TaskScheduler taskScheduler;
	private ScheduledFuture<?> job1;
	private ScheduledFuture<?> job2;
	
	@Autowired
	private ParamRepository paramRepo;
	
	@Bean
    public ThreadPoolTaskScheduler poolScheduler() {
		ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setThreadNamePrefix("scheduler-thread");
        scheduler.setPoolSize(10);
        scheduler.initialize();
        return scheduler;
    }
	
	@Override
	public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
		this.taskScheduler = poolScheduler(); //this will be used in later part of the article during refreshing the cron expression dynamically
		// TODO Auto-generated method stub
		job1(this.taskScheduler); // assign the job1 to the scheduler
		job2(this.taskScheduler); // assign the job2 to the scheduler
		taskRegistrar.setTaskScheduler(this.taskScheduler);
	}
	
	/*
	 * Bisa dipindah ke class service
	 */
	private void job1(TaskScheduler scheduler) {
		String cronExp = paramRepo.findByName("job1").getValue();
		job1 = scheduler.schedule(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						System.out.println(Thread.currentThread().getName() + " The Task1 executed at " + LocalDateTime.now().toString());
						try {
							Thread.sleep(10000);
						} catch(InterruptedException e) {
							// TODO Auto-generated catch block
		                    e.printStackTrace();
						}
					}
				}, new Trigger() {
					
					@Override
					public Date nextExecutionTime(TriggerContext triggerContext) {
						// TODO Auto-generated method stub
//						String cronExp = "0/5 * * * * ?";// Can be pulled from a db .
		                return new CronTrigger(cronExp).nextExecutionTime(triggerContext);
					}
				});
	}
	
	/*
	 * Bisa dipindah ke class service
	 */
	private void job2(TaskScheduler scheduler) {
		String cronExp = paramRepo.findByName("job2").getValue();
		job2 = scheduler.schedule(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						System.out.println(Thread.currentThread().getName() + " The Task2 executed at " + LocalDateTime.now().toString());
					}
				}, new Trigger() {
					
					@Override
					public Date nextExecutionTime(TriggerContext triggerContext) {
						// TODO Auto-generated method stub
//						String cronExp="0/1 * * * * ?";//Can be pulled from a db . This will run every minute
		                return new CronTrigger(cronExp).nextExecutionTime(triggerContext);
					}
				});
	}
	
	/*
	 * method nya bisa di pisah per job
	 */
	public void refreshCronSchedule() {
		if(job1!=null) {
			job1.cancel(true);
			job1(taskScheduler);
		}
		if(job2!=null) {
			job2.cancel(true);
			job2(taskScheduler);
		}
	}

//	@Scheduled(fixedRate = 5000)
//	public void executeTask1() {
//		System.out.println(Thread.currentThread().getName() + " The Task1 executed at " + LocalDate.now().toString());
//		try {
//			Thread.sleep(10000);
//		} catch(InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//	
//	@Scheduled(fixedRate = 1000)
//	public void executeTask2() {
//		System.out.println(Thread.currentThread().getName() + " The Task2 executed at " + LocalDate.now().toString());
//	}
}
