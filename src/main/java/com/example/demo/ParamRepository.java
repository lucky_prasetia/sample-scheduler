package com.example.demo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ParamRepository extends JpaRepository<Param, Integer>{
	
	Param findByName(String name);
}
