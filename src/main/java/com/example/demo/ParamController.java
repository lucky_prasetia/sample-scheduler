package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/param")
public class ParamController {

	@Autowired
	private ParamRepository paramRepo;
	
	@Autowired
	private ScheduledConfiguration schedule;
	
	@PostMapping("/update")
	public void update(@RequestBody Param param) {
		Param updateParam = paramRepo.findByName(param.getName());
		updateParam.setValue(param.getValue());
		paramRepo.save(updateParam);
	}
	
	@GetMapping("/restart")
	public void restart() {
		schedule.refreshCronSchedule();
	}
}
