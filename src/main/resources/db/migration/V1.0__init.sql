CREATE TABLE m_param (
	id serial PRIMARY KEY,
	name varchar(50),
	value varchar(50)
);

INSERT INTO m_param (name, value) VALUES ('job1', '0/5 * * * * ?');
INSERT INTO m_param (name, value) VALUES ('job2', '0 0 0 1/1 * ?');